package ch.rfin.util;

import java.util.Collection;
import java.util.function.*;

/**
 * Various predicates.
 * These often make working with Streams cleaner.
 * For example: {@code stream.filter(lessThan(3))} instead of
 * {@code stream.filter(n -> n < 3)}.
 * They can also make working with Comparables easier. For example,
 * {@code if (lessThan(b).test(a))} might be considered clearer than
 * {@code if (a.compareTo(b) < 0)}.
 * <p>
 * Some overloads may seem silly, like {@code zero()} and {@code zero(i)},
 * since it might seem like a method reference such as {@code Predicates::zero}
 * could be used wherever {@code Predicates.zero()} would be used. However,
 * method references can unfortunately not be treated as function literals
 * representing a functional interface instance. So, for example, while
 * {@code (Predicates::zero).negate()} is not allowed,
 * {@code Predicates.zero().negate)} is.
 * @author Christoffer Fink
 * @version 0.1.0
 */
public class Predicates {

    public static Predicate<String> emptyString() {
        return s -> s == null || s.length() == 0;
    }

    public static boolean empty(final String s) {
        return s == null || s.length() == 0;
    }

    public static <T> Predicate<Collection<T>> emptyCollection() {
        return c -> c == null || c.isEmpty();
    }

    public static <T> boolean empty(final Collection<T> c) {
        return c == null || c.isEmpty();
    }

    public static Predicate<Object> isNull() {
        return o -> o == null;
    }

    public static boolean isNull(Object o) {
        return o == null;
    }

    public static Predicate<Object> nonNull() {
        return o -> o != null;
    }

    public static boolean nonNull(Object o) {
        return o != null;
    }


    public static boolean zero(final int i) {
        return i == 0;
    }

    public static boolean positive(final int i) {
        return i > 0;
    }

    public static boolean negative(final int i) {
        return i < 0;
    }

    public static Predicate<Integer> zero() {
        return i -> i == 0;
    }

    public static Predicate<Integer> positive() {
        return i -> i > 0;
    }

    public static Predicate<Integer> negative() {
        return i -> i < 0;
    }


    /** {@code lessThan(b).test(a) ⇔ a < b}. */
    public static Predicate<Integer> lessThan(final Integer n) {
        return i -> i < n;
    }

    /** {@code greaterThan(b).test(a) ⇔ a > b}. */
    public static Predicate<Integer> greaterThan(final Integer n) {
        return i -> i > n;
    }

    /** {@code atLeast(b).test(a) ⇔ a ≥ b}. */
    public static Predicate<Integer> atLeast(final Integer n) {
        return i -> i >= n;
    }

    /** {@code atMost(b).test(a) ⇔ a ≤ b}. */
    public static Predicate<Integer> atMost(final Integer n) {
        return i -> i <= n;
    }

    /** {@code between(a, b).test(c) ⇔ a ≤ c ≤ b}. */
    public static Predicate<Integer> between(final Integer min, final Integer max) {
        return i -> min <= i && i <= max;
    }

    /** {@code outside(a, b).test(c) ⇔ c < a ∨ c > b}. */
    public static Predicate<Integer> outside(final Integer min, final Integer max) {
        return i -> i < min || i > max;
    }


    /** {@code lessThan(b).test(a) ⇔ a < b}. */
    public static <T extends Comparable<? super T>> Predicate<T> lessThan(final T b) {
        return a -> a.compareTo(b) < 0;
    }

    /** {@code atMost(b).test(a) ⇔ a ≤ b}. */
    public static <T extends Comparable<? super T>> Predicate<T> atMost(final T b) {
        return a -> a.compareTo(b) <= 0;
    }

    /** {@code greaterThan(b).test(a) ⇔ a > b}. */
    public static <T extends Comparable<? super T>> Predicate<T> greaterThan(final T b) {
        return a -> a.compareTo(b) > 0;
    }

    /** {@code atLeast(b).test(a) ⇔ a ≥ b}. */
    public static <T extends Comparable<? super T>> Predicate<T> atLeast(final T b) {
        return a -> a.compareTo(b) >= 0;
    }

    /** {@code between(a, b).test(c) ⇔ a ≤ c ≤ b}. */
    public static <T extends Comparable<? super T>> Predicate<T> between(final T min, final T max) {
        return atLeast(min).and(atMost(max));
    }

    /** {@code outside(a, b).test(c) ⇔ c < a ∨ c > b}. */
    public static <T extends Comparable<? super T>> Predicate<T> outside(final T min, final T max) {
        return between(min, max).negate();
    }


    public static class Primitive {

        public static boolean zero(final int n) {
            return n == 0;
        }

        public static boolean positive(final int n) {
            return n > 0;
        }

        public static boolean negative(final int n) {
            return n < 0;
        }

        public static IntPredicate zero() {
            return i -> i == 0;
        }

        public static IntPredicate positive() {
            return i -> i > 0;
        }

        public static IntPredicate negative() {
            return i -> i < 0;
        }

        public static IntPredicate lessThan(final int n) {
            return i -> i < n;
        }

        public static IntPredicate atMost(final int n) {
            return i -> i <= n;
        }

        public static IntPredicate greaterThan(final int n) {
            return i -> i > n;
        }

        public static IntPredicate atLeast(final int n) {
            return i -> i >= n;
        }

        public static IntPredicate between(final int min, final int max) {
            return i -> min <= i && i <= max;
        }

        public static IntPredicate outside(final int min, final int max) {
            return i -> i < min || i > max;
        }

    }

}
