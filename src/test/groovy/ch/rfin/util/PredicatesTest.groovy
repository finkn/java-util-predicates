package ch.rfin.util;

import spock.lang.*;

public class PredicatesTest extends Specification {

  def "empty string"() {
    expect:
      Predicates.empty((String) null)
      Predicates.empty("")
      !Predicates.empty("hello")
  }

  def "emptyString pred"() {
    given:
      def pred = Predicates.emptyString()
    expect:
      pred.test((String) null)
      pred.test("")
      !pred.test("hello")
  }

  def "empty collection"() {
    expect:
      Predicates.empty((Collection) null)
      Predicates.empty([])
      !Predicates.empty([1])
  }

  def "emptyCollection pred"() {
    given:
      def pred = Predicates.emptyCollection()
    expect:
      pred.test((Collection) null)
      pred.test([])
      !pred.test([1])
  }

  def "is null"() {
    given:
      def pred = Predicates.isNull()
    expect:
      Predicates.isNull(null)
      pred.test(null)
      !Predicates.isNull("")
      !Predicates.isNull("hello")
      !pred.test([])
      !pred.test([1])
  }

  def "non null"() {
    given:
      def pred = Predicates.nonNull()
    expect:
      Predicates.nonNull("")
      Predicates.nonNull("hello")
      pred.test([])
      pred.test([1])
      !Predicates.nonNull(null)
      !pred.test(null)
  }

  // --- Boxed ---

  def "zero"() {
    expect:
      !Predicates.zero(-1)
      Predicates.zero(0)
      !Predicates.zero(1)
  }

  def "zero pred"() {
    given:
      def pred = Predicates.zero()
    expect:
      !pred.test(-1)
      pred.test(0)
      !pred.test(1)
  }

  def "positive"() {
    expect:
      Predicates.positive(2)
      Predicates.positive(1)
      !Predicates.positive(0)
      !Predicates.positive(-1)
  }

  def "positive pred"() {
    given:
      def pred = Predicates.positive()
    expect:
      pred.test(2)
      pred.test(1)
      !pred.test(0)
      !pred.test(-1)
  }

  def "negative"() {
    expect:
      Predicates.negative(-2)
      Predicates.negative(-1)
      !Predicates.negative(0)
      !Predicates.negative(1)
      !Predicates.negative(2)
  }

  def "negative pred"() {
    given:
      def pred = Predicates.negative()
    expect:
      pred.test(-2)
      pred.test(-1)
      !pred.test(0)
      !pred.test(1)
      !pred.test(2)
  }

  def "lessThan pred"() {
    given:
      def pred = Predicates.lessThan(3)
    expect:
      pred.test(1)
      pred.test(2)
      !pred.test(3)
      !pred.test(4)
  }

  def "greaterThan pred"() {
    given:
      def pred = Predicates.greaterThan(3)
    expect:
      !pred.test(1)
      !pred.test(2)
      !pred.test(3)
      pred.test(4)
  }

  def "atLeast pred"() {
    given:
      def pred = Predicates.atLeast(3)
    expect:
      !pred.test(1)
      !pred.test(2)
      pred.test(3)
      pred.test(4)
  }

  def "atMost pred"() {
    given:
      def pred = Predicates.atMost(3)
    expect:
      pred.test(1)
      pred.test(2)
      pred.test(3)
      !pred.test(4)
  }

  def "between pred"() {
    given:
      def pred = Predicates.between(-1, 1)
    expect:
      !pred.test(-2)
      pred.test(-1)
      pred.test(0)
      pred.test(1)
      !pred.test(2)
  }

  def "outside pred"() {
    given:
      def pred = Predicates.outside(-1, 1)
    expect:
      pred.test(-2)
      !pred.test(-1)
      !pred.test(0)
      !pred.test(1)
      pred.test(2)
  }

  // --- Comparable ---

  def "lessThan pred comparable"() {
    given:
      def pred = Predicates.lessThan("g")
    expect:
      pred.test("e")
      pred.test("f")
      !pred.test("g")
      !pred.test("h")
  }

  def "greaterThan pred comparable"() {
    given:
      def pred = Predicates.greaterThan("g")
    expect:
      !pred.test("e")
      !pred.test("f")
      !pred.test("g")
      pred.test("h")
  }

  def "atLeast pred comparable"() {
    given:
      def pred = Predicates.atLeast("g")
    expect:
      !pred.test("e")
      !pred.test("f")
      pred.test("g")
      pred.test("h")
  }

  def "atMost pred comparable"() {
    given:
      def pred = Predicates.atMost("g")
    expect:
      pred.test("e")
      pred.test("f")
      pred.test("g")
      !pred.test("h")
  }

  def "between pred comparable"() {
    given:
      def pred = Predicates.between("c", "e")
    expect:
      !pred.test("b")
      pred.test("c")
      pred.test("d")
      pred.test("e")
      !pred.test("f")
  }

  def "outside pred comparable"() {
    given:
      def pred = Predicates.outside("c", "e")
    expect:
      pred.test("b")
      !pred.test("c")
      !pred.test("d")
      !pred.test("e")
      pred.test("f")
  }

  // --- Primitive ---

  def "zero int"() {
    expect:
      !Predicates.Primitive.zero(-1)
      Predicates.Primitive.zero(0)
      !Predicates.Primitive.zero(1)
  }

  def "zero pred int"() {
    given:
      def pred = Predicates.Primitive.zero()
    expect:
      !pred.test(-1)
      pred.test(0)
      !pred.test(1)
  }

  def "positive int"() {
    expect:
      Predicates.Primitive.positive(2)
      Predicates.Primitive.positive(1)
      !Predicates.Primitive.positive(0)
      !Predicates.Primitive.positive(-1)
  }

  def "positive pred int"() {
    given:
      def pred = Predicates.Primitive.positive()
    expect:
      pred.test(2)
      pred.test(1)
      !pred.test(0)
      !pred.test(-1)
  }

  def "negative int"() {
    expect:
      Predicates.Primitive.negative(-2)
      Predicates.Primitive.negative(-1)
      !Predicates.Primitive.negative(0)
      !Predicates.Primitive.negative(1)
      !Predicates.Primitive.negative(2)
  }

  def "negative pred int"() {
    given:
      def pred = Predicates.Primitive.negative()
    expect:
      pred.test(-2)
      pred.test(-1)
      !pred.test(0)
      !pred.test(1)
      !pred.test(2)
  }

  def "lessThan pred int"() {
    given:
      def pred = Predicates.Primitive.lessThan(3)
    expect:
      pred.test(1)
      pred.test(2)
      !pred.test(3)
      !pred.test(4)
  }

  def "greaterThan pred int"() {
    given:
      def pred = Predicates.Primitive.greaterThan(3)
    expect:
      !pred.test(1)
      !pred.test(2)
      !pred.test(3)
      pred.test(4)
  }

  def "atLeast pred int"() {
    given:
      def pred = Predicates.Primitive.atLeast(3)
    expect:
      !pred.test(1)
      !pred.test(2)
      pred.test(3)
      pred.test(4)
  }

  def "atMost pred int"() {
    given:
      def pred = Predicates.Primitive.atMost(3)
    expect:
      pred.test(1)
      pred.test(2)
      pred.test(3)
      !pred.test(4)
  }

  def "between pred int"() {
    given:
      def pred = Predicates.Primitive.between(-1, 1)
    expect:
      !pred.test(-2)
      pred.test(-1)
      pred.test(0)
      pred.test(1)
      !pred.test(2)
  }

  def "outside pred int"() {
    given:
      def pred = Predicates.Primitive.outside(-1, 1)
    expect:
      pred.test(-2)
      !pred.test(-1)
      !pred.test(0)
      !pred.test(1)
      pred.test(2)
  }

}
